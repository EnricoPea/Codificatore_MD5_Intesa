package swing_MD5;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.TransferHandler;
import javax.swing.text.JTextComponent;

public final class ClipBoard extends MouseAdapter {

    private final Clipboard clipBoard;
    private final JPopupMenu popup;
    private final JTextComponent textComponent;
    private JMenuItem copy;
    private JMenuItem paste;
    private JMenuItem clear;
    private JMenuItem cut;
    private final TransferHandler handler;

    public ClipBoard(JTextComponent textComponent) {
        popup = new JPopupMenu();
        // Toolkit toolkit = Toolkit.getDefaultToolkit();
        clipBoard = Toolkit.getDefaultToolkit().getSystemClipboard();
        this.textComponent = textComponent;
        handler = textComponent.getTransferHandler();
        createMenu();
    }

    protected void createMenu() {
        copy = new JMenuItem("Copy");
        paste = new JMenuItem("Paste");
        cut = new JMenuItem("Cut");
        clear = new JMenuItem("Clear");
    }

    public void addPasteFunction() {
        paste.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Transferable clipboardContent = clipBoard.getContents(this);
                if (clipboardContent != null && (clipboardContent.isDataFlavorSupported(DataFlavor.stringFlavor))) {
                    try {
                        textComponent.setText(
                                textComponent.getText() + clipboardContent.getTransferData(DataFlavor.stringFlavor));
                    } catch (Exception ex) {
                    }
                }
            }
        });
        popup.add(paste);
    }

    public void addCopyFunction() {
        copy.addActionListener((ActionEvent e) -> {
            handler.exportToClipboard(textComponent, clipBoard, TransferHandler.COPY);
        });
        popup.add(copy);
    }

    public void addClearFunction() {
        popup.addSeparator();
        popup.add(clear);
    }

    public void addCutFunction() {
        cut.addActionListener((ActionEvent e) -> {
            handler.exportToClipboard(textComponent, clipBoard, TransferHandler.MOVE);
            
        });
        popup.add(cut);
    }

    public void addPasteFunction(String nameItem) {
        paste.setText(nameItem);
        addPasteFunction();
    }

    public void addCopyFunction(String nameItem) {
        copy.setText(nameItem);
        addCopyFunction();
    }

    public void addClearFunction(String nameItem) {
        clear.setText(nameItem);
        addClearFunction();
    }

    public void addCutFunction(String menuItem) {
        cut.setText(menuItem);
        addCutFunction();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        showPopup(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        showPopup(e);
    }

    private void showPopup(MouseEvent e) {
        if (e.isPopupTrigger()) {
            if (textComponent.getText().length() == 0) {
                copy.setEnabled(false);
                cut.setEnabled(false);
            } else {
                copy.setEnabled(true);
                copy.setEnabled(true);
            }
            popup.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    public JPopupMenu getPopup() {
        return popup;
    }

    public static ClipBoard installForComponent(JTextComponent c) {
        ClipBoard cpb = new ClipBoard(c);
        c.addMouseListener(cpb);
        return cpb;
    }
}
